<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TicketsSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i < 20; $i++) {
            DB::table('tickets')->insert([
                'user_id' => 2,
                'title' => str_random(10),
                'description' => str_random(20),
            ]);

        }
    }
}
